Panopoly
========

Panopoly is a powerful base distribution of Drupal 9, that includes many
usability improvements, and powerful additional features.

It can be used as a starting point for a new Drupal site, or as the base
to create your own Drupal 9 distribution.

See [the project page on Drupal.org](https://www.drupal.org/project/panopoly)
for more information.

## Usage

You need a couple of dependencies first:

- [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

After that you can create your new project via:

    composer create-project panopoly/panopoly-composer-template:9.x-dev somedir --no-interaction

... replacing 'somedir' with the directory to create your new site.

